<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 22:11
 */

namespace Partime\Tests\Model;


use Partime\Model\BasketRow;
use Partime\Model\Product;

class BasketRowTest extends \PHPUnit_Framework_TestCase
{
    public function testBasketRow()
    {
        $product = \Mockery::mock(Product::class);
        $quantity = 4;

        $basketRow = new BasketRow($product, $quantity);

        $this->assertSame($product, $basketRow->getProduct());
        $this->assertSame($quantity, $basketRow->getQuantity());
    }
}
