<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

use Partime\Controllers\Basket as BasketController;
use Partime\Controllers\Checkout;
// use Partime\Model\Product;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    protected $products, $basket, $invoice;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->basket = new BasketController;
    }

    /**
     * @Given I can sell the products:
     */
    public function iCanSellTheProducts(TableNode $table)
    {
        foreach ($table->getHash() as $key => $product) {
            $this->products[$product["name"]] = [$product["unit price"], $product["tax rate"]];
        }

        // throw new PendingException();
    }

    /**
     * @When I add :arg1 apples to my basket
     */
    public function iAddApplesToMyBasket($arg1)
    {   
        $this->basket->addProductToBasket('apples', $arg1); 
    }

    /**
     * @When I add :arg1 papers to my basket
     */
    public function iAddPapersToMyBasket($arg1)
    {
        $this->basket->addProductToBasket('papers', $arg1);
    }

    /**
     * @When I add :arg1 toys to my basket
     */
    public function iAddToysToMyBasket($arg1)
    {
        $this->basket->addProductToBasket('toys', $arg1);
    }

    /**
     * @Then my basket contains:
     */
    public function myBasketContains(TableNode $table)
    {
        if (count(array_diff($table->getHash(), $this->basket->getBasketDetails())) !== 0) {
                throw new Exception("Carts not equal");

            var_dump($table->getHash());
            var_dump($this->basket->getBasketDetails());                 
        }              
    }

    /**
     * @When I remove :arg1 papers from my basket
     */
    public function iRemovePapersFromMyBasket($arg1)
    {
        $this->basket->removeProductFromBasket('papers', $arg1);
    }

    /**
     * @Then my invoice contains:
     */
    public function myInvoiceContains(TableNode $table)
    {   
        $checkout = new Checkout($this->basket->basket);
        
        $this->invoice = $checkout->generateInvoice();

        $invoiceDetails = [];

        foreach($this->invoice->getInvoiceRows() as $row) {
            $invoiceDetails[] = ['name' => $row->getName(), 'quantity' => $row->getQuantity(), 'total' => $row->getQuantity*$row->getPrice()];
        } 

        if (count(array_diff($table->getHash(), $invoiceDetails)) !== 0) {
                throw new Exception("Invoice not correct");
                
            var_dump($table->getHash());
            var_dump($checkout->generateInvoice());                 
        }      
    }

    /**
     * @Then my gross total is :arg1
     */
    public function myGrossTotalIs($arg1)
    {
        if((float)$this->invoice->getGrossTotal() !== (float)$arg1) {
            throw new Exception("Actual gross total: " . $this->invoice->getGrossTotal());
        }
    }

    /**
     * @Then my tax total is :arg1
     */
    public function myTaxTotalIs($arg1)
    {
        if((float)$this->invoice->getTax() !== (float)$arg1) {
            throw new Exception("Actual tax total: " . $this->invoice->getTax());
        }
    }

    /**
     * @Then my net total is :arg1
     */
    public function myNetTotalIs($arg1)
    {
        if((float)$this->invoice->getNetTotal() !== (float)$arg1) {
            throw new Exception("Actual net total: " . $this->invoice->getNetTotal());
        }
    }
}
