Feature: Checkout Controller
  I want to be able to calculate the final bill for my basket

  Background:
    Given I can sell the products:
      | name   | unit price | tax rate |
      | apples | 0.10       | 0        |
      | papers | 2.50       | 0.05     |
      | toys   | 24.99      | 0.2      |

  Scenario: When I fill a basket and check it out the bill I get is correct
    When I add 1 apples to my basket
    And I add 2 papers to my basket
    And I add 1 toys to my basket
    Then my invoice contains:
      | name   | quantity | total |
      | apples | 1        | 0.1   |
      | papers | 2        | 5     |
      | toys   | 1        | 24.99 |
    And my gross total is 30.09
    And my tax total is 5.25
    And my net total is 35.34