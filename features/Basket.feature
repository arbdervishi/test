Feature: Basket Controller
  I want to be able to add and remove items from my basket

  Background:
    Given I can sell the products:
    | name   | unit price | tax rate |
    | apples | 0.10       | 0        |
    | papers | 2.50       | 0.05     |
    | toys   | 24.99      | 0.2      |

  Scenario: Adding an item to a basket and listing it
    When I add 1 apples to my basket
    And I add 2 papers to my basket
    And I add 1 toys to my basket
    Then my basket contains:
      | name   | quantity |
      | apples | 1        |
      | papers | 2        |
      | toys   | 1        |

  Scenario: I can remove an item from my basket
    When I add 1 apples to my basket
    And I add 2 papers to my basket
    And I add 1 toys to my basket
    And I remove 1 papers from my basket
    Then my basket contains:
      | name   | quantity |
      | apples | 1        |
      | papers | 1        |
      | toys   | 1        |