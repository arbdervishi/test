<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 22:17
 */

namespace Partime\Tests\Model;


use Partime\Model\Basket;
use Partime\Model\BasketRow;

class BasketTest extends \PHPUnit_Framework_TestCase
{
    public function testRowsCanBeAddedToBasket()
    {
        $row1 = \Mockery::mock(BasketRow::class);
        $row2 = \Mockery::mock(BasketRow::class);
        $row3 = \Mockery::mock(BasketRow::class);

        $basket = new Basket();
        $basket->addRow($row1);
        $basket->addRow($row2);
        $basket->addRow($row3);

        $list = $basket->listRows();
        $this->assertCount(3, $list);
        foreach($list as $key => $row) {
            $this->assertSame(${'row'.($key + 1)}, $row);
        }
    }

    public function testRowsCanBeDeleted()
    {
        $row1 = \Mockery::mock(BasketRow::class);
        $row2 = \Mockery::mock(BasketRow::class);
        $row3 = \Mockery::mock(BasketRow::class);

        $basket = new Basket();
        $basket->addRow($row1);
        $basket->addRow($row2);
        $basket->addRow($row3);

        $basket->removeRow($row2);

        $list = $basket->listRows();
        $this->assertCount(2, $list);
        $this->assertSame($row1, $list[0]);
        $this->assertSame($row3, $list[1]);

    }
}
