<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 22:31
 */

namespace Partime\Tests\Model;


use Partime\Model\InvoiceRow;

class InvoiceRowTest extends \PHPUnit_Framework_TestCase
{
    public function testInvoiceRowStoresData()
    {
        $name = 'rowName';
        $quantity = 1;
        $price = 1.5;
        $tax = 0.3;

        $invoiceRow = new InvoiceRow($name, $quantity, $price, $tax);

        $this->assertSame($name, $invoiceRow->getName());
        $this->assertSame($quantity, $invoiceRow->getQuantity());
        $this->assertSame($price, $invoiceRow->getPrice());
        $this->assertSame($tax, $invoiceRow->getTax());
    }
}
