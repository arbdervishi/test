<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 22:52
 */

namespace Partime\Controllers;

use Partime\Model\Basket as BasketModel;
use Partime\Model\InvoiceRow;
use Partime\Model\Invoice;

class Checkout
{
	protected $invoiceRows, $grossTotal = 0, $tax = 0, $netTotal = 0;

	public function __construct(BasketModel $basket)
	{
		$this->basket = $basket;

		foreach ($this->basket->listRows() as $row) {
			$unitPrice = $row->getProduct()->getUnitPrice();
			$taxRate = $row->getProduct()->getTaxRate();
			$taxAmount = $taxRate * $unitPrice;

			$this->grossTotal += $unitPrice * $row->getQuantity();
			$this->tax += $taxAmount*$row->getQuantity();

			$this->invoiceRows[] = new InvoiceRow($row->getProduct()->getName(), $row->getQuantity(), $unitPrice, $taxRate);
		}
	}

    public function generateInvoice()
    {
    	$invoice = new Invoice($this->invoiceRows, round($this->grossTotal, 2), round($this->tax, 2), round($this->grossTotal + $this->tax, 2));


    	return $invoice;
    }
}