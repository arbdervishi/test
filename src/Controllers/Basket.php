<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 22:51
 */

namespace Partime\Controllers;

use Partime\Model\Basket as BasketModel;
use Partime\Model\BasketRow;
use Partime\Model\Product;

class Basket
{
	public $basket;

	public function __construct()
	{
		$this->basket = new BasketModel;

		$this->products = [
    		'apples' => [0.10, 0], 
    		'papers' => [2.50, 0.05], 
    		'toys' => [24.99, 0.2]
		];
	}

    public function addProductToBasket($name, $qty)
    {
    	$product = new Product($name, $this->products[$name][0], $this->products[$name][1]);

    	$this->basket->addRow(new BasketRow($product, $qty));
    }

    public function removeProductFromBasket($name, $qty)
    {
    	$product = new Product($name, $this->products[$name][0], $this->products[$name][1]);
    	
    	foreach ($this->basket->listRows() as $r) {
    		if($r->getProduct()->getName() == $name) {
    			$row = $r;
    		}
    	}

    	$this->basket->removeRow($row);

    	$newQty = $row->getQuantity() - $qty;

    	if( $newQty > 0) {
    		$this->basket->addRow(new BasketRow($product, $newQty));
    	}
    }

    public function getBasketDetails()
    {
    	$basketDetails = [];

    	foreach ($this->basket->listRows() as $row) {
    		$basketDetails[] = ['name' => $row->getProduct()->getName(), 'quantity' => $row->getQuantity()];
    	}

    	return $basketDetails;
    }
}