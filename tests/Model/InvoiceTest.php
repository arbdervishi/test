<?php
/**
 * Created by PhpStorm.
 * User: Gil Kirschner
 * Date: 22/08/2016
 * Time: 22:34
 */

namespace Partime\Tests\Model;


use Partime\Model\Invoice;

class InvoiceTest extends \PHPUnit_Framework_TestCase
{
    public function testInvoiceStoresData()
    {
        $invoiceRows = array();
        $grossTotal = 10.04;
        $tax = 2.01;
        $netTotal = 12.05;

        $invoice = new Invoice($invoiceRows, $grossTotal, $tax, $netTotal);

        $this->assertSame($invoiceRows, $invoice->getInvoiceRows());
        $this->assertSame($grossTotal, $invoice->getGrossTotal());
        $this->assertSame($tax, $invoice->getTax());
        $this->assertSame($netTotal, $invoice->getNetTotal());
    }
}
